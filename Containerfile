FROM docker.io/rust:1.67-bookworm AS build

COPY ./Cargo.toml /
COPY ./Cargo.lock /
COPY ./src /src

RUN cargo build --locked --release

FROM debian:bookworm

# dependencies under npm come from
# https://www.howtogeek.com/devops/how-to-run-puppeteer-and-headless-chrome-in-a-docker-container/
RUN apt-get update && apt-get install -y --no-install-recommends \
    nodejs \
    npm \
    fonts-liberation \
    gconf-service \
    libappindicator1 \
    libasound2 \
    libatk1.0-0 \
    libcairo2 \
    libcups2 \
    libfontconfig1 \
    libgbm-dev \
    libgdk-pixbuf2.0-0 \
    libgtk-3-0 \
    libicu-dev \
    libjpeg-dev \
    libnspr4 \
    libnss3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libpng-dev \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    xdg-utils \
    && rm -rf /var/lib/apt/lists/*

RUN useradd --system --create-home --user-group \
    --uid 900 --home-dir /usr/lib/mntnwttrbot \
    --shell /bin/false \
    mntnwttrbot

COPY ./capture/package.json /usr/lib/mntnwttrbot/package.json
COPY ./capture/package-lock.json /usr/lib/mntnwttrbot/package-lock.json

RUN mkdir -p /var/lib/mntnwttrbot/cache && chown -R mntnwttrbot:mntnwttrbot /var/lib/mntnwttrbot

RUN cd /usr/lib/mntnwttrbot/ && npm ci

COPY ./capture/main.js /usr/lib/mntnwttrbot/main.js

COPY --from=build /target/release/mntnwttrbot /usr/bin/mntnwttrbot

USER mntnwttrbot

ENTRYPOINT ["/usr/bin/mntnwttrbot"]

CMD ["-c", "/etc/mntnwttrbot/settings.toml", "-m", "var/lib/mntnwttrbot/mountains.json"]
