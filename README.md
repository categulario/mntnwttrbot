# Mountain weather forecast bot

Ask this bot for a mountain's weather forecast.

## How to run

You need a config file like this:

```toml
# settings.toml
token = "some random string"
```

Then you can run it with this:

    cargo run --bin mntnwttrbot -- -c settings.toml -m mountains.json

## Run the web scrapper

This is a necessary step to build `mountains.json`.

First make a release build with:

    cargo build --release

Then run the scrapper and specify output path:

    RUST_LOG=get_mountains=debug time target/release/get_mountains -o mountains.json

There are over 11,000 mountains, at 20 mountains per second it should take
around 9 minutes. This value can be tweaked for better performance but keep in
mind that the server might panic or something.

## Run the screen capture service

Just this command:

    node main.js -c ./mntns/ -m Pico-de-Orizaba -a 5610

With this arguments:

* `-c --cache-path` for a directory where the images will be stored
* `-m --mountain` for the code of the mountain to download
* `-a --altitude` for the altitude

## Caching model

To prevent making too many requests for the same mountain in a 24 hour period of
time a cache is created. This cache stores weather captures and uses them until
they expire. Expiry is provided by the website, who reports the time in secons
for which a report is valid.

When a capture is retrieved a 'delete' task is scheduled for it at expiry time.
Since shutting down the bot would make it forget about current delete tasks and
leave a permanent cache the bot unconditionaly clears the cache on start.
