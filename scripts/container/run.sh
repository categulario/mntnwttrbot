#!/bin/bash

__dir=$(dirname $(realpath $0))

container=mntnwttrbot
image=registry.gitlab.com/categulario/mntnwttrbot/mntnwttrbot

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="-it $image $@"
fi

podman run --rm \
    --uidmap 900:0:1 --uidmap 0:1:900 \
    --volume $__dir/../../settings_container.toml:/etc/mntnwttrbot/settings.toml:U \
    --volume $__dir/../../mountains.json:/var/lib/mntnwttrbot/mountains.json:U \
    $name \
    $cmd
