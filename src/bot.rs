use std::path::PathBuf;
use std::time::Duration;

use anyhow::{Result, Error};
use teloxide::prelude::*;
use teloxide::adaptors::DefaultParseMode;
use teloxide::types::{
    ReplyMarkup, InlineKeyboardButton, InlineKeyboardButtonKind, InputFile,
};
use tokio::process::Command;
use strfmt::strfmt;
use serde::Deserialize;

use crate::settings::Settings;
use crate::core::{Mountain, Query, Response, respond};
use crate::msgs::*;
use crate::teloxide_bridge::clean;

pub struct AppData {
    pub settings: Settings,
    pub mountain_list: Vec<Mountain>,
}

#[derive(Debug, Deserialize)]
struct CaptureResponse {
    update_time: u64,
}

async fn get_capture(mountain: &Mountain, altitude: u16, settings: &Settings) -> Result<PathBuf> {
    let capture_path = {
        let mut p = settings.capture_cache_dir
            .join(&mountain.code).join(altitude.to_string());
        p.set_extension("png");
        p
    };

    if !capture_path.exists() {
        log::debug!("Calling capture module to get capture of: {capture_path:?}");

        let cmd = Command::new(&settings.node_path)
            .arg(&settings.capture_script_path)
            .arg("-c").arg(&settings.capture_cache_dir)
            .arg("-m").arg(&mountain.code)
            .arg("-a").arg(altitude.to_string())
            .arg("--container")
            .output().await.unwrap();

        let stdout = String::from_utf8_lossy(&cmd.stdout);
        let stderr = String::from_utf8_lossy(&cmd.stderr).to_string();

        if cmd.status.success() {
            let res: CaptureResponse = serde_json::from_str(&stdout)?;
            let cache_path = capture_path.clone();

            tokio::spawn(async move {
                let update_time = res.update_time;
                log::debug!("Timer set to clear cache for {cache_path:?} in {update_time} secs");
                tokio::time::sleep(Duration::from_secs(update_time)).await;

                if let Err(e) = tokio::fs::remove_file(&cache_path).await {
                    log::error!("{e}");
                } else {
                    log::debug!("Cleared cache for {cache_path:?}");
                }
            });
        } else {
            return Err(Error::msg(stderr));
        }
    } else {
        log::debug!("Using cached capture: {capture_path:?}");
    }

    Ok(capture_path)
}

async fn send_report(bot: &DefaultParseMode<Bot>, chat_id: ChatId, path: PathBuf, mountain: &Mountain, altitude: u16, index: usize) -> Result<()> {
    let code = clean(&mountain.code);
    let mntnname = clean(&mountain.name);

    let msg = strfmt(MSG_SUCCESS_MOUNTAIN_FORECAST, &[
        ("mountain".to_string(), mntnname),
        ("altitude".to_string(), altitude.to_string()),
        ("link".to_string(), format!("https://www\\.mountain\\-forecast\\.com/peaks/{code}/forecasts/{altitude}")),
    ].into_iter().collect())?;

    // send the picture with the weather and a caption with info
    bot
        .send_photo(chat_id, InputFile::file(path))
        .caption(msg)
        .send().await?;

    // Create an inline keyboard with other altitudes from the same mountain
    let inline_keyboard = ReplyMarkup::inline_kb(
        mountain.altitudes.chunks(3).map(|altitudes| {
            altitudes.iter().map(|altitude| {
                InlineKeyboardButton::new(
                    format!("⬆️ {altitude}"),
                    InlineKeyboardButtonKind::CallbackData(format!("m:{index}:{altitude}"))
                )
            }).collect::<Vec<_>>()
        })
    );

    // send a message with some info
    bot
        .send_message(chat_id, MSG_OTHER_ALTITUDES)
        .reply_markup(inline_keyboard)
        .send().await?;

    Ok(())
}

async fn respond_logic(bot: &DefaultParseMode<Bot>, chat_id: ChatId, mountain: &Mountain, altitude: u16, mountain_index: usize, settings: &Settings) -> Result<()> {
    let wait_msg = bot
        .send_message(chat_id, MSG_GETTING_CAPTURE).send().await.unwrap();

    let cap = get_capture(mountain, altitude, settings).await;

    bot
        .delete_message(chat_id, wait_msg.id)
        .send().await.unwrap();

    match cap {
        Ok(path) => {
            send_report(bot, chat_id, path, mountain, altitude, mountain_index).await.unwrap();
        },
        Err(e) => {
            bot
                .send_message(chat_id, MSG_CAPTURE_FAILED)
                .send().await.unwrap();

            log::error!("{e}");
        }
    }

    Ok(())
}

async fn message_handler(bot: DefaultParseMode<Bot>, message: Message, data: &'static AppData) -> Result<()> {
    match Query::try_from(&message) {
        Ok(q) => match respond(&q, &data.mountain_list) {
            Response::Hello => {
                bot
                    .send_message(message.chat.id, MSG_HELLO)
                    .await?;
            }

            Response::Empty => {
                bot
                    .send_message(message.chat.id, MSG_NO_MOUNTAIN_FOUND)
                    .reply_to_message_id(message.id)
                    .await?;
            }

            Response::MountainList(mountains) => {
                let inline_keyboard = ReplyMarkup::inline_kb(
                    mountains.into_iter().map(|(i, m)| {
                        let altitude = m.altitudes[0];

                        vec![InlineKeyboardButton::new(
                            format!("🗻 {} ({})", m.name, altitude),
                            InlineKeyboardButtonKind::CallbackData(format!("m:{i}:{altitude}"))
                        )]
                    })
                );

                bot
                    .send_message(message.chat.id, MSG_MANY_MOUNTAINS_FOUND)
                    .reply_to_message_id(message.id)
                    .reply_markup(inline_keyboard)
                    .await?;
            }

            Response::WeatherReport { mountain, altitude, index } => {
                respond_logic(
                    &bot,
                    message.chat.id,
                    &mountain,
                    altitude,
                    index,
                    &data.settings,
                ).await.unwrap();
            }
        },
        Err(_) => {
            bot
                .send_message(message.chat.id, MSG_INVALID_QUERY)
                .reply_to_message_id(message.id)
                .await?;
        }
    }

    Ok(())
}

async fn callback_handler(bot: DefaultParseMode<Bot>, callback_query: CallbackQuery, data: &'static AppData) -> Result<()> {
    let message = callback_query.message;
    let payload = callback_query.data;

    if let (Some(msg), Some(payload)) = (message, payload) {
        bot.delete_message(msg.chat.id, msg.id).send().await.unwrap();

        match Query::parse_inline_data(&payload) {
            Ok(query) => {
                if let Query::Button { mountain_index, altitude } = query {
                    let mountain = &data.mountain_list[mountain_index];

                    respond_logic(
                        &bot,
                        msg.chat.id,
                        mountain,
                        altitude,
                        mountain_index,
                        &data.settings,
                    ).await.unwrap();
                }
            }
            Err(description) => {
                bot.send_message(msg.chat.id, description).send().await.unwrap();
            }
        }
    }

    Ok(())
}

async fn run(data: &'static AppData) -> Result<()> {
    log::info!("Starting bot...");

    let bot = Bot::new(&data.settings.token)
        .parse_mode(teloxide::types::ParseMode::MarkdownV2);

    let handler = dptree::entry()
        .branch(Update::filter_message().endpoint(message_handler))
        .branch(Update::filter_callback_query().endpoint(callback_handler));

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![data])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;

    Ok(())
}

#[tokio::main]
pub async fn bot_main(data: AppData) -> Result<()> {
    let static_ref: &'static _ = Box::leak(Box::new(data));

    run(static_ref).await
}
