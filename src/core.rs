//! The core logic of the bot. Everything else are implementation details.
//!
//! The logic is very simple. A function that takes input from the user and
//! returns the expected output from the bot in the form of a Response enum
//! variant.
use std::fmt;

use serde::{Serialize, Deserialize};
use regex::Regex;

use crate::text::normalize;
use crate::msgs::MSG_OLD_BUTTON_PRESSED;

lazy_static! {
    static ref MNTN_CALLBACK_REGEX: Regex = Regex::new(r"m:(?P<index>\d+):(?P<altitude>\d+)").unwrap();
}

/// The Response given by the bot
#[derive(Debug, PartialEq)]
pub(crate) enum Response {
    Hello,

    /// A mountain and altitude were specified, so the response is a full report
    /// with the option of choosing some other altitude to further query the
    /// weather.
    WeatherReport {
        mountain: Mountain,
        altitude: u16,
        index: usize,
    },

    /// The query was not specific enough so a list of possible mountains is
    /// returned.
    MountainList(Vec<(usize, Mountain)>),

    /// No match was found, sugest something else
    Empty,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Query {
    /// Very first message
    Start,

    /// The user sent a text message
    Text {
        msg: String,
    },

    /// The user clicked on a button
    Button {
        mountain_index: usize,
        altitude: u16,
    },
}

impl Query {
    pub fn text(msg: &str) -> Query {
        Query::Text {
            msg: msg.into(),
        }
    }

    pub fn parse_inline_data(data: &str) -> Result<Query, &'static str> {
        if let Some(caps) = MNTN_CALLBACK_REGEX.captures(data) {
            Ok(Query::Button {
                mountain_index: (caps["index"]).parse().map_err(|_| MSG_OLD_BUTTON_PRESSED)?,
                altitude: (caps["altitude"]).parse().map_err(|_| MSG_OLD_BUTTON_PRESSED)?,
            })
        } else {
            Err(MSG_OLD_BUTTON_PRESSED)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Mountain {
    pub name: String,
    pub normalized_name: String,
    pub code: String,
    pub altitudes: Vec<u16>,
}

impl fmt::Display for Mountain {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.name)
    }
}

pub(crate) fn respond(query: &Query, mountain_list: &[Mountain]) -> Response {
    match query {
        Query::Start => Response::Hello,
        Query::Text { msg, .. } => {
            let matches: Vec<_> = mountain_list
                .iter().enumerate()
                .filter(|(_, m)| m.normalized_name.contains(&normalize(msg)))
                .collect();

            match matches.len() {
                0 => Response::Empty,
                1 => {
                    let (index, mountain) = matches[0];

                    Response::WeatherReport {
                        mountain: mountain.clone(),
                        altitude: mountain.altitudes[0],
                        index,
                    }
                }
                _ => Response::MountainList(
                    matches.into_iter().take(5)
                        .map(|(i, m)| (i, m.clone()))
                        .collect()
                ),
            }
        }

        Query::Button { .. } => unimplemented!(),
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use super::*;

    fn make_mountain(name: &str) -> Mountain {
        Mountain {
            name: name.into(),
            normalized_name: normalize(name),
            code: name.into(),
            altitudes: vec![4283],
        }
    }

    #[test]
    fn with_only_one_match_specific_mountain_is_returned() {
        let query = Query::text("a");
        let a = make_mountain("a");
        let mountain_list = vec![
            a.clone(), make_mountain("b"), make_mountain("c"),
        ];

        assert_eq!(respond(&query, &mountain_list), Response::WeatherReport {
            mountain: a,
            altitude: 4283,
            index: 0,
        });
    }

    #[test]
    fn normalized_search() {
        let query = Query::text("á.Ma");
        let a = make_mountain("ama");
        let mountain_list = vec![a.clone()];

        assert_eq!(respond(&query, &mountain_list), Response::WeatherReport {
            mountain: a,
            altitude: 4283,
            index: 0,
        });
    }

    #[test]
    fn with_multiple_matches_options_are_returned() {
        let query = Query::text("a");
        let a = make_mountain("a");
        let aa = make_mountain("aa");
        let mountain_list = vec![
            a.clone(), aa.clone(), make_mountain("b"), make_mountain("c"),
        ];

        assert_eq!(respond(&query, &mountain_list), Response::MountainList(vec![
            (0, a), (1, aa),
        ]));
    }

    #[test]
    fn parse_inline_data() {
        assert_eq!(Query::parse_inline_data("m:123:1020").unwrap(), Query::Button {
            mountain_index: 123,
            altitude: 1020,
        });
    }
}
