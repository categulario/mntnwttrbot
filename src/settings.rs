use std::path::PathBuf;

use serde::Deserialize;

#[derive(Clone, Deserialize)]
#[serde(default)]
pub struct Settings {
    pub token: String,

    pub node_path: String,

    pub capture_script_path: String,

    pub capture_cache_dir: PathBuf,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            token: "123456".to_owned(),
            node_path: "node".to_owned(),
            capture_script_path: "main.js".to_owned(),
            capture_cache_dir: PathBuf::from("capture_cache"),
        }
    }
}
