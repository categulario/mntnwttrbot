use unidecode::unidecode;

/// return a normalized version of s for the purposes of matching.
///
/// This processes are considered:
/// - lowercase
/// - remove accents or turn them into ascii chars if possible
/// - remove symbols
pub fn normalize(s: &str) -> String {
    unidecode(s).to_lowercase().replace(|c: char| {
        !c.is_alphanumeric()
    }, "")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn normalization() {
        assert_eq!(normalize("ándalE tu"), "andaletu");
    }
}
