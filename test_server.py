from pprint import pprint

from flask import Flask, request

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def print_body():
    json = request.get_json()
    pprint(json)
    return 'ok'
